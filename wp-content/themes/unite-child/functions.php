<?php
function my_theme_enqueue_styles() {

    $parent_style = 'unite-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );



// Our custom post type function
function custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Films', 'Post Type General Name', 'unite-child' ),
            'singular_name'       => _x( 'Film', 'Post Type Singular Name', 'unite-child' ),
            'menu_name'           => __( 'Films', 'unite-child' ),
            'parent_item_colon'   => __( 'Parent Film', 'unite-child' ),
            'all_items'           => __( 'All Films', 'unite-child' ),
            'view_item'           => __( 'View Film', 'unite-child' ),
            'add_new_item'        => __( 'Add New Film', 'unite-child' ),
            'add_new'             => __( 'Add New', 'unite-child' ),
            'edit_item'           => __( 'Edit Film', 'unite-child' ),
            'update_item'         => __( 'Update Film', 'unite-child' ),
            'search_items'        => __( 'Search Film', 'unite-child' ),
            'not_found'           => __( 'Not Found', 'unite-child' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'unite-child' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'films', 'unite-child' ),
            'description'         => __( 'Film news and reviews', 'unite-child' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'taxonomies'          => array( 'genres' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        );
         
        // Registering your Custom Post Type
        register_post_type( 'films', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'custom_post_type', 0 );



    
//=================== adding the genre taxonomy ==================
    //hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_genres_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it topics for your posts
 
function create_genres_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Genres', 'taxonomy general name' ),
    'singular_name' => _x( 'Genre', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Genres' ),
    'all_items' => __( 'All Genres' ),
    'parent_item' => __( 'Parent Genre' ),
    'parent_item_colon' => __( 'Parent Genre:' ),
    'edit_item' => __( 'Edit Genre' ), 
    'update_item' => __( 'Update Genre' ),
    'add_new_item' => __( 'Add New Genre' ),
    'new_item_name' => __( 'New Genre Name' ),
    'menu_name' => __( 'Genres' ),
  );    
 
// Now register the taxonomy
 
  register_taxonomy('genres',array('films'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'film' ),
  ));
 
}

//=================== adding the genre taxonomy ended ==================

//=================== adding the country taxonomy ==================
    //hook into the init action and call create_book_taxonomies when it fires
    add_action( 'init', 'create_countries_hierarchical_taxonomy', 0 );
 
    //create a custom taxonomy name it topics for your posts
     
    function create_countries_hierarchical_taxonomy() {
     
    // Add new taxonomy, make it hierarchical like categories
    //first do the translations part for GUI
     
      $labels = array(
        'name' => _x( 'Countries', 'taxonomy general name' ),
        'singular_name' => _x( 'Country', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Countries' ),
        'all_items' => __( 'All Countries' ),
        'parent_item' => __( 'Parent Country' ),
        'parent_item_colon' => __( 'Parent Country:' ),
        'edit_item' => __( 'Edit Country' ), 
        'update_item' => __( 'Update Country' ),
        'add_new_item' => __( 'Add New Country' ),
        'new_item_name' => __( 'New Country Name' ),
        'menu_name' => __( 'Countries' ),
      );    
     
    // Now register the taxonomy
     
      register_taxonomy('countries',array('films'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'film' ),
      ));
     
    }
    
    //=================== adding the country taxonomy ended ==================


    //=================== adding the actors taxonomy ==================
    //hook into the init action and call create_book_taxonomies when it fires
    add_action( 'init', 'create_actors_hierarchical_taxonomy', 0 );
 
    function create_actors_hierarchical_taxonomy() {
     
    // Add new taxonomy, make it hierarchical like categories
    //first do the translations part for GUI
     
      $labels = array(
        'name' => _x( 'Actors', 'taxonomy general name' ),
        'singular_name' => _x( 'Actor', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Actors' ),
        'all_items' => __( 'All Actors' ),
        'parent_item' => __( 'Parent Actor' ),
        'parent_item_colon' => __( 'Parent Actor:' ),
        'edit_item' => __( 'Edit Actor' ), 
        'update_item' => __( 'Update Actor' ),
        'add_new_item' => __( 'Add New Actor' ),
        'new_item_name' => __( 'New Actor Name' ),
        'menu_name' => __( 'Actors' ),
      );    
     
    // Now register the taxonomy
     
      register_taxonomy('actors',array('films'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'film' ),
      ));
     
    }
    
    //=================== adding the actors taxonomy ended ==================
    
    //=================== adding the year taxonomy ==================
    //hook into the init action and call create_book_taxonomies when it fires
    add_action( 'init', 'create_years_hierarchical_taxonomy', 0 );
 
    function create_years_hierarchical_taxonomy() {
     
    // Add new taxonomy, make it hierarchical like categories
    //first do the translations part for GUI
     
      $labels = array(
        'name' => _x( 'Years', 'taxonomy general name' ),
        'singular_name' => _x( 'Year', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Years' ),
        'all_items' => __( 'All Years' ),
        'parent_item' => __( 'Parent Year' ),
        'parent_item_colon' => __( 'Parent Year:' ),
        'edit_item' => __( 'Edit Year' ), 
        'update_item' => __( 'Update Year' ),
        'add_new_item' => __( 'Add New Year' ),
        'new_item_name' => __( 'New Year Name' ),
        'menu_name' => __( 'Years' ),
      );    
     
    // Now register the taxonomy
     
      register_taxonomy('years',array('films'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'film' ),
      ));
     
    }
    
    //=================== adding the actors taxonomy ended ==================


    // creating a shortode to display films post
    
    function getLastFiveFilms() { 
        ob_start(); 
        $data = '<div class="testiScroller">'; 
            $query = new WP_Query('post_type=films&showposts=5&orderby=desc'); 
                while( $query->have_posts() ):$query->the_post(); 
                $data .= '<div class="singleTestimonial" id="testomial-'.get_the_ID().'">'; 
                  
                    $data .= '<div class="clientDetails"><b>- '.get_the_title().'</b><div>('.get_the_excerpt().')</div></div><br />'; 
                $data .= '</div>'; 
            endwhile; wp_reset_postdata(); 
        $data .= '</div>'; 
        return $data; 
    } 
    add_shortcode('fadingTestimonials','getLastFiveFilms');

    //
?>



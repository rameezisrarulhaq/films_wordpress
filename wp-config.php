<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3=<}.]~EqA}LPKRWn]d5B<4=ltAmDazcXNzDL@J40HKZf}d_R<08j>s(is:k_7~H');
define('SECURE_AUTH_KEY',  'Pe=7gW]qROAtkUt? z?YePa4*5D[};Ve{=[Q;(pe$Xht*x];t(KB(Qjw_)Re/}4Z');
define('LOGGED_IN_KEY',    'nsriPKMf.ef)yq=s& [7@8QXn:znwlambTH;+m]4VE+%m/K*ePy>AeRIQuvl9t`w');
define('NONCE_KEY',        '~qTfNW {q#(V$TnIz<x/*v`w$]@YFHX2e]Z2vn5+fWUdt8STq*Us[7ymLo^bZQQW');
define('AUTH_SALT',        'acM4{J==srMZ<]T>*/=I1Hb)~ J8~=ug|{Ah:A<hX`Df8lZmHw=%9&0Nd,}r1V.>');
define('SECURE_AUTH_SALT', 'UzWJD#Z}OGBcoL*88[,,,^^U1`o=xhy2rJcR4~+6oZE}F(dtd^Ecgiq=Dfjolt;O');
define('LOGGED_IN_SALT',   't+JOxO#%vXt8~y4t8u/1^Z9 .p[51 _[;+O.[`[GC]Y5n#Ae2}3 BveNIy+|4%,T');
define('NONCE_SALT',       '|[_+a<$`IpMMD!@;MAXJnx#)$u?Vz>EFS0/ujW^$Zz?dZ&yWqyi{gSz^&vOSx,W_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
